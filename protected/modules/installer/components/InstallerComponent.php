<?php

Yii::import('installer.models.InstallerCheckPrerequisite');

class InstallerComponent extends CApplicationComponent {

    public function init() {
        parent::init();

        $this->checkPrerequisites();
    }

    public function checkPrerequisites() {
        $checker = new InstallerCheckPrerequisite;
        if (!$checker->validate()) {
            Yii::app()->controller->redirect(array('/installer/site/index'));
        }
    }

}
