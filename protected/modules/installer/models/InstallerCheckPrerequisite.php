<?php

class InstallerCheckPrerequisite extends CFormModel {

    public $path_asset;

    public function rules() {
        return array(
            array('path_asset', 'checkAsset'),
            array('path_asset', 'checkFolder'),
            array('path_asset', 'checkWritable'),
        );
    }

    public function checkFolder($attribute) {
        
    }

    public function checkWritable($attribute) {
        
    }

    public function checkAsset($attribute) {
        try {
            Yii::app()->assetManager->getBasePath();
        } catch (CException $e) {
            $this->addError($attribute, $e->getMessage());
        }
    }

}
