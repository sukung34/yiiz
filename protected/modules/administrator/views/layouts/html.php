<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo Yii::app()->name; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php Yii::app()->clientScript->registerCssFile($this->module->assetUrl . '/css/style.css'); ?>
    </head>
    <body>
        <?php echo $content; ?>
    </body>
</html>