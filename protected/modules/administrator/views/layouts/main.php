<?php $this->beginContent('administrator.views.layouts.html'); ?>
<?php
$this->widget('booster.widgets.TbNavbar', array(
    'brand' => CHtml::image($this->module->assetUrl . '/images/yiiz-logo.png', 'Yiiiz logo', array('class' => 'yiiz-logo')) . ' Yiiz Control Panel',
    'brandUrl' => $this->createUrl('/administrator'),
    'fixed' => false,
    'fluid' => false,
    'items' => array(
        array(
            'class' => 'booster.widgets.TbMenu',
            'type' => 'navbar',
            'items' => array(
                array(
                    'label' => 'Configuration',
                    'items' => array(
                        array(
                            'label' => 'Database',
                            'url' => array('/administrator/database/index'),
                        ),
                    ),
                ),
                array(
                    'label' => 'Logout',
                    'url' => array('/administrator/site/logout'),
                ),
            ),
        ),
    ),
));
?>
<div class="container">
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>