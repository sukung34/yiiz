<div class="center">
    <div><h1><?php echo CHtml::image($this->module->assetUrl . '/images/yiiz-logo.png', 'Yiiz Logo', array('class' => 'yiiz-logo')); ?> Yiiz Control Panel</h1></div>
</div> 
<div class="col-sm-offset-3 col-sm-6">
    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'type' => 'horizontal',
        'focus' => array($model, 'username'),
    ));
    ?>
    <?php echo $form->textFieldGroup($model, 'username'); ?>
    <?php echo $form->passwordFieldGroup($model, 'secret'); ?>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9"> 
            <?php echo $form->errorSummary($model); ?>
            <div>
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'label' => 'Login',
                    'context' => 'primary',
                    'size' => 'small',
                ));
                ?>
                <?php
                $this->widget('booster.widgets.TbButton', array(
                    'label' => 'Back to Frontend',
                    'url' => Yii::app()->homeUrl,
                    'size' => 'small',
                ));
                ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>