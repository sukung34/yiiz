<div class="container">
    <h2>Error <?php echo $code; ?></h2>
    <div class="btn-toolbar">
        <?php
        $this->widget('booster.widgets.TbButtonGroup', array(
            'size' => 'small',
            'buttons' => array(
                array(
                    'label' => 'Back to Home',
                    'url' => array('/administrator'),
                ),
            ),
        ));
        ?>
    </div>
    <hr/>
    <div class="error">
        <?php echo CHtml::encode($message); ?>
    </div>
    <code><?php echo $file; ?> <?php echo $line; ?></code>
    <pre><?php echo $trace; ?></pre>
</div>