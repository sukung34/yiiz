<div class="page-header">
    <h1>Database</h1>
</div>
<div class="btn-toolbar">
    <?php
    $this->widget('booster.widgets.TbButtonGroup', array(
        'buttons' => array(
        ),
    ));
    ?>
</div>
<?php foreach ($dbs as $db): ?>
    <?php
    $this->widget('booster.widgets.TbPanel', array(
        'title' => $db->id,
        'headerIcon' => 'hdd',
        'context' => 'primary',
        'headerButtons' => array(
            array(
                'class' => 'booster.widgets.TbButtonGroup',
                'context' => 'primary',
                'buttons' => array(
                    array(
                        'label' => 'New Connection',
                        'context' => 'primary',
                        'url' => array('create'),
                    ),
                )
            ),
        )
    ));
    ?>
<?php endforeach; ?>