<?php

Yii::import('administrator.components.*');
Yii::import('administrator.models._base.*');
Yii::import('administrator.models.*');

class AdministratorModule extends CWebModule {

    public $assetUrl;
    public $defaultController = 'site';
    public $layout = 'administrator.views.layouts.main';

    public function init() {
        parent::init();
        Yii::app()->setComponent('user', array(
            'class' => 'AdministratorWebUser',
            'allowAutoLogin' => true,
                ), false);
        Yii::app()->setComponent('db', array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/data/core.db',
                ), false);
        Yii::app()->setComponent('errorHandler', array(
            'errorAction' => '/administrator/site/error',
        ));

        $this->setModules(array(
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => 'lol',
                'ipFilters' => array('*'),
                'generatorPaths' => array(
                    'ext.giix.generators',
                ),
            ),
        ));
        $this->assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets', false, -1, YII_DEBUG);
    }

}
