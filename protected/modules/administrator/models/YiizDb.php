<?php

Yii::import('application.modules.administrator.models._base.BaseYiizDb');

class YiizDb extends BaseYiizDb {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getIsPrimary() {
        return $this->id === 'db';
    }

}
