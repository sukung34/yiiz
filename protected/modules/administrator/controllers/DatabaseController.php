<?php

class DatabaseController extends AdministratorController {

    public function actionIndex() {
        $dbs = YiizDb::model()->findAll();
        $this->render('index', array(
            'dbs' => $dbs,
        ));
    }

}
