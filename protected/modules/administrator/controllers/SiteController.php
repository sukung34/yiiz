<?php

class SiteController extends AdministratorController {

    public function accessRules() {
        return array_merge(array(
            array(
                'allow',
                'users' => array('*'),
                'actions' => array(
                    'login',
                    'logout',
                    'error',
                ),
            ),
                ), parent::accessRules());
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionError() {
        $this->layout = 'administrator.views.layouts.html';
        $error = Yii::app()->errorHandler->error;
        if ($error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionLogin() {
        $this->layout = 'administrator.views.layouts.login';
        $model = new AdministratorLoginForm;
        $data = Yii::app()->request->getPost('AdministratorLoginForm');
        if (isset($data)) {
            $model->attributes = $data;
            if ($model->login()) {
                $this->redirect(array('/administrator'));
            }
        }
        $this->render('login', array(
            'model' => $model,
        ));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(array('/administrator'));
    }

}
