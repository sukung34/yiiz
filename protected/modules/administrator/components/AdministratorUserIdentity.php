<?php

class AdministratorUserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate() {
        $user = YiizUser::model()->findByAttributes(array(
            'username' => $this->username,
            'secret' => YiizUser::encrypt($this->password),
        ));
        if (isset($user)) {
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->id;
        } else {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
