<?php

class AdministratorLoginForm extends CFormModel {

    public $username;
    public $secret;
    private $_identity;

    public function attributeLabels() {
        return array(
            'username' => 'Username',
            'secret' => 'Password',
        );
    }

    public function rules() {
        return array(
            array('username, secret', 'required'),
            array('username, secret', 'length', 'min' => 4, 'max' => 64),
        );
    }

    public function login() {
        if ($this->validate()) {
            if ($this->_identity === null) {
                $this->_identity = new AdministratorUserIdentity($this->username, $this->secret);
                $this->_identity->authenticate();
            }
            if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
                Yii::app()->user->login($this->_identity, 3600);
                return true;
            } else {
                $this->addError('secret', 'Invalid password.');
                return false;
            }
        }
    }

}
