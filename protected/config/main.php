<?php

// uncomment the following to define a path alias
Yii::setPathOfAlias('booster', dirname(__FILE__) . '/../extensions/yii-booster');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Web Application',
    'preload' => array(
        'booster',
    ),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix.components.*'
    ),
    'modules' => array(
        'administrator' => array(),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'lol',
            'ipFilters' => array('*'),
            'generatorPaths' => array(
                'ext.giix.generators',
            ),
        ),
    ),
    // application components
    'components' => array(
        'booster' => array(
            'class' => 'ext.yii-booster.components.Booster',
        ),
        'messages' => array(
            'extensionBasePaths' => array(
                'giix' => 'ext.giix.messages',
            ),
        ),
        'user' => array(
// enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
        'db' => array(
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/core.db',
        ),
        // uncomment the following to use a MySQL database
        /*
          'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=testdrive',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          ),
         */
        'errorHandler' => array(
// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
